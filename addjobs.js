const Queue = require('bull')
const queue = new Queue('fetchFeed')
const low = require('lowdb')
const FileSync = require('lowdb/adapters/FileSync')
const adapter = new FileSync('db.json')
const db = low(adapter)

;(async ()=>{
	const users = db.read().get('users').value()
	for (const user of users) {
		await queue.add({user})
		console.log(user.author)
		// break
	}
	
})()