const Queue = require('bull')
const jobQueue = new Queue('feedAddJob')

jobQueue.add({}, {
    repeat: {
        cron: "5 */11 * * *"
    }
})