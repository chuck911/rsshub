const fs = require('fs')

const feeds = fs.readFileSync('./blogs.csv', 'utf-8')
    .split(/\r?\n/)
    .map(line => line.split(',').map(s=>s.trim()))

for (const feed of feeds) {
    if (feed[3]>500 && feed[4]>500) {
        fs.appendFileSync('valid_blogs.csv', feed.join(',')+'\n')
    }
}