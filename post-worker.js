const fs = require('fs')
const needle = require('needle')
const randomize = require('randomatic')
const Redis = require("ioredis")
const redis = new Redis()
const md5 = require('md5')
const Turndown = require('turndown')
const RSSParser = require('rss-parser')
const parser = new RSSParser()
const Queue = require('bull')
const queue = new Queue('fetchFeed')
const turndownOptions = {codeBlockStyle:'fenced'}
const turndowner = new Turndown(turndownOptions)
turndowner.addRule('bold', {
	filter: ['strong', 'b'],
	replacement: content => '**'+content+'** '
})
turndowner.addRule('img', {
	filter: 'img',
	replacement: (content, node) => {
		var alt = node.alt || ''
		var src = node.getAttribute('src') || node.getAttribute('data-src') || ''
		return src ? '![' + alt + ']' + '(' + src + ')' : ''
	}
})


const token = 'va.o7vrhCUuK_3cqyYlKQfPSdVzm1vTiQiP.e8tVcQ.FYx3hH9'
const options = {headers:{
	Authorization: 'Bearer '+token
}}

async function fetchFeed(user) {
	let rss = await parser.parseURL(user.feed)
	const items = rss.items.reverse()
	for (const item of items) {
		const added = await redis.sismember('mengsbbs:feeds', md5(item.link))
		console.log(added)
		if (added) continue
		let content = item['content:encoded']||item.content||item.description||item.summary
		content = turndowner.turndown(content)
		content = content.replace(/((?:\r?\n)\d{1,2}\s)/g, '')
		// console.log(content)
		const date = item.isoDate.substring(0,19).replace('T', ' ')
		const postData = {
			body: content,
			categoryID: 2,
			format: "markdown",
			name: item.title,
			insertUserID: user.id,
			dateInserted: date
		}
		try {
			const res = await needle('post','http://mengsbbs.cn/api/v2/discussions', postData, options)
			console.log(res.body.discussionID)
			if (res.body.discussionID) {
				await redis.sadd('mengsbbs:feeds', md5(item.link))
				fs.appendFileSync('./links.log', item.link+'\n')
				await needle('post', 'http://data.zz.baidu.com/urls?site=mengsbbs.cn&token=RlATKhNLSwKLpxa9&type=mip' ,res.body.url, {
					headers: {'Content-Type': 'text/plain'}
				})
			}else {
				console.log(res.body, item.link)
			}
		} catch (error) {
			console.error(error)
		}
		// break
	}
}

queue.process(async (job)=>{
	const user = job.data.user
	return fetchFeed(user)
})


