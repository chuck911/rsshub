const Queue = require('bull')
const fetchQueue = new Queue('fetchFeed')
const jobQueue = new Queue('feedAddJob')
const low = require('lowdb')
const FileSync = require('lowdb/adapters/FileSync')
const adapter = new FileSync('db.json')
const db = low(adapter)

jobQueue.process(async (job)=>{
	const users = db.read().get('users').value()
	for (const user of users) {
		await fetchQueue.add({user})
	}
})